﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DTO
{
    public class CookbookDto
    {
        public CookbookDto()
        {
            Recipes = new List<RecipeDto>();
        }

        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public DateTime Date { get; set; }
        public int UserId { get; set; }
        public string Username { get; set; }
        public List<RecipeDto> Recipes { get; set; }
    }
}
