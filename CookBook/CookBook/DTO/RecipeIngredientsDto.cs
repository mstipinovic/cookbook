﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class RecipeIngredientsDto
    {
        public int RecipeId { get; set; }
        public int IngredientId { get; set; }
        [Required]
        public double Amount { get; set; }
        public IngredientDto Ingredient { get; set; }
        public RecipeDto Recipe { get; set; }
    }
}
