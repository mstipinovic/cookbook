﻿using BLL.Service;
using CookBook.ViewModels;
using DTO;
using System.Collections.Generic;
using System.Linq;
using System.Web.Helpers;
using System.Web.Mvc;

namespace CookBook.Controllers
{
    public class UserController : Controller
    {
        private UserService _service;

        public UserController()
        {
            _service = new UserService();
        }
        
        public ActionResult Index()
        {
            var users = _service.GetAll();
            var vm = new List<UserViewModel>();
            foreach (var user in users)
            {
                vm.Add(new UserViewModel(user));
            }

            return View(vm);
        }
        

        private UserProfileViewModel Get(int id)
        {
            var user = _service.Get(id);
            if (user.Recipes.Count >= 3)
                user.Recipes.RemoveRange(3, user.Recipes.Count - 3);

            var vm = new UserProfileViewModel(user);

            return vm;
        }

        public ActionResult GetProfile()
        {
            if (Session["UserId"] == null)
                return RedirectToAction("Login", "Account");

            var vm = Get((int)Session["UserId"]);

            return View(vm);
        }

        public ActionResult GetPublicProfile(int id)
        {
            if (Session["UserId"]!=null && (int)Session["UserId"]==id)
                return RedirectToAction("GetProfile","User");

            var vm = Get(id);

            return View(vm);
        }

        private UserRecipesViewModel GetAllUserRecipes(int id)
        {
            var user = _service.Get(id);
            var vm = new UserRecipesViewModel(user);
            foreach (var recipe in user.Recipes)
            {
                vm.Recipes.Add(new RecipeViewModel(recipe));
            }

            return vm;
        }

        public ActionResult GetAllUserRecipes()
        {
            if (Session["UserId"] == null)
                return RedirectToAction("Login", "Account");

            var vm = GetAllUserRecipes((int)Session["UserId"]);

            return View(vm);
        }

        public ActionResult GetAllPublicUserRecipes(int id)
        {
            var vm = GetAllUserRecipes(id);

            return View(vm);
        }

        private UserCookbooksViewModel GetAllUserCookbooks(int id)
        {
            var user = _service.Get(id);
            var vm = new UserCookbooksViewModel
            {
                User = new UserViewModel(user),
                Cookbooks = user.Cookbooks
            };

            return vm;
        }

        public ActionResult GetAllUserCookbooks()
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");

            var vm = GetAllUserCookbooks((int)Session["UserId"]);

            return View(vm);
        }

        public ActionResult GetAllPublicUserCookbooks(int userId)
        {
            var vm = GetAllUserCookbooks(userId);

            return View(vm);
        }


        public ActionResult GetFavoriteRecipes()
        {
            if (Session["UserId"] == null)
                return RedirectToAction("Login","Account");

            var user = _service.Get((int)Session["UserId"]);

            var vm = new UserRecipesViewModel(user);
            foreach (var recipe in user.FavoriteRecipes)
            {
                vm.Recipes.Add(new RecipeViewModel(recipe));
            }

            return View(vm);
        }

        public ActionResult AddRandomRecipe(int recipeId)
        {
            if (Session["UserId"] == null)
                return RedirectToAction("Login", "Account");

            _service.AddRandomRecipe((int)Session["UserId"], recipeId);

            return RedirectToAction("Get", "Recipe", new { id = recipeId });
        }

        public ActionResult AddFavoriteRecipe(int recipeId)
        {
            if (Session["UserId"] == null)
                return RedirectToAction("Login","Account");
          
            _service.AddFavoriteRecipe((int)Session["UserId"], recipeId);

            return RedirectToAction("Get", "Recipe", new { id = recipeId });
        }

        public ActionResult RemoveFavoriteRecipe(int recipeId)
        {
            _service.RemoveFavoriteRecipe((int)Session["UserId"], recipeId);

            return RedirectToAction("GetFavoriteRecipes","User");
        }

        [HttpGet]
        public ActionResult Edit()
        {
            if (Session["UserId"] == null)
                return RedirectToAction("Login", "Account");

            var user = _service.Get((int)Session["UserId"]);
            var vm = new EditUserViewModel(user);

            return View(vm);
        }

        [HttpPost]
        public ActionResult Edit(EditUserViewModel vm)
        {
            if (!ModelState.IsValid)
                return View(vm);

            var users = _service.CheckIfExist(vm.User.Name, (int)Session["UserId"]);
            if (users.Any())
                {
                    ModelState.AddModelError(string.Empty, "User with selected username already exist!");
                    return View(vm);
                }

            var user = new UserDto { Id = vm.User.Id, Username = vm.User.Name };

            if (vm.NewPassword == null)
                user.Password = vm.OldPassword;
            else
                user.Password = Crypto.HashPassword(vm.NewPassword);

            _service.Update(user);

            return RedirectToAction("GetProfile", "User");
        }

        [HttpGet]
        public ActionResult Delete()
        {
            if (Session["UserId"] == null)
                return RedirectToAction("Login", "Account");

            var user = _service.Get((int)Session["UserId"]);
            var vm = new UserViewModel(user);

            return View(vm);
        }

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed()
        {
            _service.Delete((int)Session["UserId"]);
            Session["UserId"] = null;

            return RedirectToAction("HomePage", "Home");
        }
    }
}