﻿using BLL.Service;
using CookBook.ViewModels;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using static Model.Enums.CategoryEnum;
using System.Linq;

namespace CookBook.Controllers
{
    public class IngredientController : Controller
    {
        private IngredientService _service;

        public IngredientController()
        {
            _service = new IngredientService();
        }

        public ActionResult Index()
        {
            var ingredients = _service.GetAll();
            var vm = new List<IngredientViewModel>();
            foreach (var ingredient in ingredients)
            {
                vm.Add(new IngredientViewModel { Ingredient = ingredient });
            }

            return View(vm);
        }

        [HttpGet]
        public ActionResult Create()
        {
            if (Session["UserId"] == null)
                return RedirectToAction("Login", "Account");

            var vm = new IngredientViewModel();

            return View(vm);
        }

        [HttpPost]
        public ActionResult Create(IngredientViewModel vm)
        {
            if (!ModelState.IsValid)
                return View(vm);

            var ingredientCheck = _service.CheckIfExist(vm.Ingredient.Id, vm.Ingredient.Name, vm.Ingredient.Measure);
            if (ingredientCheck.Any())
            {
                ModelState.AddModelError(string.Empty, "Ingredient already exist");
                return View(vm);
            }

            _service.Create(vm.Ingredient);

            return RedirectToAction("Index", "Ingredient");
        }

        public ActionResult Get(int id)
        {
            var ingredient = _service.Get(id);
            var vm = new IngredientViewModel { Ingredient = ingredient };

            return View(vm);
        }

        public ActionResult GetAllByCategory(int categoryId)
        {
            var ingredients = _service.GetByCategory(categoryId);
            var vm = new IngredientCategoryViewModel();
            vm.Category = new CategoryViewModel { Name = Enum.GetName(typeof(IngredientCategory), categoryId) };
            foreach (var ingredient in ingredients)
            {
                vm.Ingredients.Add(ingredient);
            }

            return View(vm);
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            if (Session["UserId"] == null)
                return RedirectToAction("Login", "Account");

            var ingredient = _service.Get(id);
            var vm = new IngredientViewModel { Ingredient = ingredient };

            return View(vm);
        }

        [HttpPost]
        public ActionResult Edit(IngredientViewModel vm)
        {
            if (!ModelState.IsValid)
                return View(vm);

            var ingredient = _service.CheckIfExist(vm.Ingredient.Id,vm.Ingredient.Name, vm.Ingredient.Measure);
            if (ingredient.Any())
            {
                ModelState.AddModelError(string.Empty, "Ingredient already exist");
                return View(vm);
            }

            _service.Update(vm.Ingredient);

            return RedirectToAction("Index", "Ingredient");
        }

        public ActionResult Delete(int id)
        {
            if (Session["UserId"] == null)
                return RedirectToAction("Login", "Account");

            var ingredient = _service.Get(id);
            var vm = new IngredientViewModel { Ingredient = ingredient };

            return View(vm);
        }

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            _service.Delete(id);

            return RedirectToAction("Index", "Ingredient");
        }
    }
}