﻿using DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CookBook.ViewModels
{
    public class CookbookViewModel
    {
        public CookbookDto Cookbook { get; set; }
    }
}