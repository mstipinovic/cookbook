﻿using DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CookBook.ViewModels
{
    public class RecipeViewModel
    {
        public RecipeViewModel()
        {
        }

        public RecipeViewModel(RecipeDto recipe)
        {
            Id = recipe.Id;
            Name = recipe.Name;
        }

        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
    }
}