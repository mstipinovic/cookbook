﻿using DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CookBook.ViewModels
{
    public class UserCookbooksViewModel
    {
        public UserCookbooksViewModel()
        {
            Cookbooks = new List<CookbookDto>();
        }

        public List<CookbookDto> Cookbooks { get; set; }
        public UserViewModel User { get; set; }
    }
}