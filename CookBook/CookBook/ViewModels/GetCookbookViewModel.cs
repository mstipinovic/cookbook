﻿using DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CookBook.ViewModels
{
    public class GetCookbookViewModel
    {
        public CookbookDto Cookbook { get; set; }
        public RecipeDto Recipe { get; set; }
    }
}