﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CookBook.ViewModels
{
    public class RecipeCategoryViewModel
    {
        public RecipeCategoryViewModel()
        {
            Recipes = new List<RecipeViewModel>();
        }

        public List<RecipeViewModel> Recipes { get; set; }
        public CategoryViewModel Category { get; set; }
    }
}