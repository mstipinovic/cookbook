﻿using DAL.Context;
using Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;

namespace DAL.Repository
{
    public class UserRepository
    {
        public void Create(User user)
        {
            using (var context = new CookBookContext())
            {
                context.Users.Add(user);
                context.SaveChanges();
            }
        }

        public User Get(string username)
        {
            using (var context = new CookBookContext())
            {
                return context.Users.FirstOrDefault(x => x.Username.Equals(username));
            }
        }

        public User Get(int id)
        {
            using (var context = new CookBookContext())
            {
                return context.Users.Include("Recipes").Include("RandomAndFavoriteRecipes").Include("Cookbooks").FirstOrDefault(x => x.Id == id);
            }
        }

        public string GetUsername(int id)
        {
            using (var context = new CookBookContext())
            {
                return context.Users.Single(x => x.Id == id).Username;
            }
        }

        public List<User> GetAll()
        {
            using (var context = new CookBookContext())
            {
                return context.Users.OrderBy(x => x.Username.ToLower()).ToList();
            }
        }


        public List<User> CheckIfExist(string username, int id)
        {
            using (var context = new CookBookContext())
            {
                return context.Users.Where(x => x.Username.Equals(username) && x.Id!=id).ToList();
            }
        }

        public UserRecipes GetUserRecipe(int userId, int recipeId)
        {
            using (var context = new CookBookContext())
            {
                return context.UserRecipes.FirstOrDefault(x => x.UserId==userId && x.RecipeId==recipeId);
            }
        }

        public void AddUserRecipe(UserRecipes recipe)
        {
            using (var context = new CookBookContext())
            {
                context.UserRecipes.Add(recipe);
                context.SaveChanges();
            }
        }

        public UserRecipes GetOldestUserRecipe()
        {
            using (var context=new CookBookContext())
            {
                var id = context.UserRecipes.Where(x=>x.Random==true).Select(x => x.Id).ToList().Min();

                return context.UserRecipes.FirstOrDefault(x=>x.Id==id);
            }
        }

        public void UpdateUserRecipe(UserRecipes recipe)
        {
            using (var context = new CookBookContext())
            {
                context.Entry(recipe).State=EntityState.Modified;
                context.SaveChanges();
            }
        }

        public void RemoveUserRecipe(UserRecipes recipe)
        {
            using (var context = new CookBookContext())
            {
                context.Entry(recipe).State=EntityState.Deleted;
                context.SaveChanges();
            }
        }

        public void Update(User user)
        {
            using (var context = new CookBookContext())
            {
                context.Entry(user).State = EntityState.Modified;
                context.SaveChanges();
            }
        }

        public void Delete(int id)
        {
            using (var context = new CookBookContext())
            {
                var user = context.Users.Single(x => x.Id == id);

                context.Cookbooks.RemoveRange(context.Cookbooks.Where(x => x.UserId == id));
                context.SaveChanges();

                context.Entry(user).State = EntityState.Deleted;
                context.SaveChanges();
            }
        }
    }
}
