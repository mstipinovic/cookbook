﻿using DAL.Context;
using Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repository
{
    public class CookbookRepository
    {
        public void Create(Cookbook cookbook)
        {
            using (var context=new CookBookContext())
            {
                context.Cookbooks.Add(cookbook);
                context.SaveChanges();
            }
        }

        public Cookbook Get(int id)
        {
            using (var context=new CookBookContext())
            {
                return context.Cookbooks.Include("Recipes").FirstOrDefault(x=>x.Id==id);
            }
        }

        public List<Cookbook> GetAll()
        {
            using (var context = new CookBookContext())
            {
                return context.Cookbooks.OrderByDescending(x=>x.Date).ToList();
            }
        }

        public List<Cookbook> GetAllByUser(int userId)
        {
            using (var context = new CookBookContext())
            {
                return context.Cookbooks.Where(x=>x.UserId==userId).ToList();
            }
        }

        public List<int> GetAllCookbookRecipes(int id)
        {
            using (var context=new CookBookContext())
            {
                return context.CookbookRecipes.Where(x => x.CookbookId == id).Select(x=>x.RecipeId).ToList();
            }
        }

        public void AddRecipe(CookbookRecipes recipe)
        {
            using (var context=new CookBookContext())
            {
                context.CookbookRecipes.Add(recipe);
                context.SaveChanges();
            }
        }

        public void RemoveRecipe(int cookbookId, int recipeId)
        {
            using (var context=new CookBookContext())
            {
                var recipe = context.CookbookRecipes.SingleOrDefault(x => x.CookbookId == cookbookId && x.RecipeId == recipeId);
                context.Entry(recipe).State = EntityState.Deleted;
                context.SaveChanges();
            }
        }

        public void Update(Cookbook cookbook)
        {
            using (var context = new CookBookContext())
            {
                context.Entry(cookbook).State = EntityState.Modified;
                context.SaveChanges();
            }
        }

        public void Delete(int id)
        {
            using (var context = new CookBookContext())
            {
                var cookbook = context.Cookbooks.Single(x=>x.Id==id);
                context.Entry(cookbook).State = EntityState.Deleted;
                context.SaveChanges();
            }
        }
    }
}
