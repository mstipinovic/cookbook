namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateIngredient : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Ingredient", "Name", c => c.String());
            AddColumn("dbo.Ingredient", "Measure", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Ingredient", "Measure");
            DropColumn("dbo.Ingredient", "Name");
        }
    }
}
