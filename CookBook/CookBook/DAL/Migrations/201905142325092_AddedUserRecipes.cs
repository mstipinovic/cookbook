namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedUserRecipes : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserRecipes",
                c => new
                    {
                        UserId = c.Int(nullable: false),
                        RecipeId = c.Int(nullable: false),
                        Random = c.Boolean(nullable: false),
                        Favorite = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => new { t.UserId, t.RecipeId })
                .ForeignKey("dbo.User", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserRecipes", "UserId", "dbo.User");
            DropIndex("dbo.UserRecipes", new[] { "UserId" });
            DropTable("dbo.UserRecipes");
        }
    }
}
