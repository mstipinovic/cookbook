namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedDateToRecipe : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Recipe", "Date", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Recipe", "Date");
        }
    }
}
