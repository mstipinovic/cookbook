namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedCookbook : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CookbookRecipes",
                c => new
                    {
                        CookbookId = c.Int(nullable: false),
                        RecipeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.CookbookId, t.RecipeId })
                .ForeignKey("dbo.Cookbook", t => t.CookbookId, cascadeDelete: true)
                .ForeignKey("dbo.Recipe", t => t.RecipeId, cascadeDelete: true)
                .Index(t => t.CookbookId)
                .Index(t => t.RecipeId);
            
            CreateTable(
                "dbo.Cookbook",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Date = c.DateTime(nullable: false),
                        UserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.User", t => t.UserId, cascadeDelete: false)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.CookbookRecipes", "RecipeId", "dbo.Recipe");
            DropForeignKey("dbo.Cookbook", "UserId", "dbo.User");
            DropForeignKey("dbo.CookbookRecipes", "CookbookId", "dbo.Cookbook");
            DropIndex("dbo.Cookbook", new[] { "UserId" });
            DropIndex("dbo.CookbookRecipes", new[] { "RecipeId" });
            DropIndex("dbo.CookbookRecipes", new[] { "CookbookId" });
            DropTable("dbo.Cookbook");
            DropTable("dbo.CookbookRecipes");
        }
    }
}
