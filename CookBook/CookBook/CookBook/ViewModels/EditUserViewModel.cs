﻿using DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CookBook.ViewModels
{
    public class EditUserViewModel
    {
        public EditUserViewModel()
        {
        }

        public EditUserViewModel(UserDto user)
        {
            User = new UserViewModel(user);
            OldPassword = user.Password;
        }

        public UserViewModel User { get; set; }
        public string OldPassword { get; set; }
        [StringLength(8, MinimumLength = 6, ErrorMessage ="The password must have at least 6 and maximum 8 characters.")]
        public string NewPassword { get; set; }
    }
}