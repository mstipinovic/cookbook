﻿using DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CookBook.ViewModels
{
    public class GetRecipeViewModel
    {
        public GetRecipeViewModel()
        {
            Ingredients = new List<RecipeIngredientsDto>();
        }

        public GetRecipeViewModel(RecipeDto recipe)
        {
            Recipe = new BasicRecipeDataViewModel(recipe);
            UserId = recipe.UserId;
            Username = recipe.Username;
            Ingredients = new List<RecipeIngredientsDto>();
            Ingredients = recipe.Ingredients;
            Description = recipe.Description;
        }

        public BasicRecipeDataViewModel Recipe { get; set; }
        public int UserId { get; set; }
        public string Username { get; set; }
        public List<RecipeIngredientsDto> Ingredients { get; set; }
        public string Description { get; set; }
    }
}