﻿using DTO;
using System;
using System.ComponentModel;
using static Model.Enums.CategoryEnum;

namespace CookBook.ViewModels
{
    public class BasicRecipeDataViewModel
    {
        public BasicRecipeDataViewModel()
        {
        }

        public BasicRecipeDataViewModel(RecipeDto recipe)
        {
            Id = recipe.Id;
            Name = recipe.Name;
            Date = recipe.Date;
            Category = recipe.Category;
        }

        public int Id { get; set; }
        [DisplayName("Name")]
        public string Name { get; set; }
        [DisplayName("Date")]
        public DateTime Date { get; set; }
        [DisplayName("Category")]
        public RecipeCategory Category { get; set; }
    }
}