﻿using DTO;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CookBook.ViewModels
{
    public class EditRecipeViewModel
    {
        public EditRecipeViewModel()
        {
            Ingredients = new List<IngredientDto>();
        }
      
        public RecipeDto Recipe { get; set; }
        public RecipeIngredientsDto NewIngredient { get; set; }
        public List<IngredientDto> Ingredients { get; set; }
    }
}