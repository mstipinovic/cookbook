﻿using DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CookBook.ViewModels
{
    public class UserViewModel
    {
        public UserViewModel()
        {
        }

        public UserViewModel(UserDto user)
        {
            Id = user.Id;
            Name = user.Username;
        }

        public int Id { get; set; }
        [Required]
        [DisplayName("Username")]
        public string Name { get; set; }
    }
}