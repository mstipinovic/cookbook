﻿using DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CookBook.ViewModels
{
    public class UserProfileViewModel
    {
        public UserProfileViewModel()
        {
            Recipes = new List<BasicRecipeDataViewModel>();
        }

        public UserProfileViewModel(UserDto user)
        {
            User = new UserViewModel(user);
            Recipes = new List<BasicRecipeDataViewModel>();
            foreach (var recipe in user.Recipes)
            {
                Recipes.Add(new BasicRecipeDataViewModel(recipe));
            }
        }

        public UserViewModel User { get; set; }
        public List<BasicRecipeDataViewModel> Recipes { get; set; }
    }
}