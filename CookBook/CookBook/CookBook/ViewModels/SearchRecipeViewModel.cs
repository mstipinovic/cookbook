﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CookBook.ViewModels
{
    public class SearchRecipeViewModel
    {
        public SearchRecipeViewModel()
        {
            SearchCategories = new List<SelectListItem>
            {
                new SelectListItem { Text="Name", Value="1"},
                new SelectListItem { Text = "Food Category", Value = "2"}
            };

            Recipes = new List<RecipeViewModel>();
        }

        public List<RecipeViewModel> Recipes { get; set; }
        public List<SelectListItem> SearchCategories { get; set; }
    }
}