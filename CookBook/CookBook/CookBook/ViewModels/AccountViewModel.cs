﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace CookBook.ViewModels
{
    public class AccountViewModel
    {
        [Required]
        [DisplayName("Username")]
        public string Username { get; set; }
        [Required]
        [DisplayName("Password")]
        [StringLength(8, MinimumLength = 6, ErrorMessage ="The password must have at least 6 and maximum 8 characters.")]
        public string Password { get; set; }
    }
}