﻿using DTO;
using System.ComponentModel.DataAnnotations;

namespace CookBook.ViewModels
{
    public class IngredientViewModel
    {
        public IngredientDto Ingredient { get; set; }
    }
}