﻿using DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CookBook.ViewModels
{
    public class UserRecipesViewModel
    {
        public UserRecipesViewModel()
        {
            Recipes = new List<RecipeViewModel>();
        }

        public UserRecipesViewModel(UserDto user)
        {
            User = new UserViewModel(user);
            Recipes = new List<RecipeViewModel>();
         }

        public UserViewModel User { get; set; }
        public List<RecipeViewModel> Recipes { get; set; }
    }
}