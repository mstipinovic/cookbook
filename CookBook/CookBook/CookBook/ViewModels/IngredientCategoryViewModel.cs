﻿using DTO;
using System.Collections.Generic;

namespace CookBook.ViewModels
{
    public class IngredientCategoryViewModel
    {
        public IngredientCategoryViewModel()
        {
            Ingredients = new List<IngredientDto>();
        }

        public List<IngredientDto> Ingredients { get; set; }
        public CategoryViewModel Category { get; set; }
    }
}