﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace CookBook.ViewModels
{
    public class IndexRecipeViewModel
    {
        public IndexRecipeViewModel()
        {
            SearchCategories = new List<SelectListItem>
            {
                new SelectListItem { Text="Name", Value="1"},
                new SelectListItem { Text = "Food Category", Value = "2"}
            };

            Recipes = new List<BasicRecipeDataViewModel>();
        }

        public List<BasicRecipeDataViewModel> Recipes { get; set; }
        public List<SelectListItem> SearchCategories { get; set; }
    }
}