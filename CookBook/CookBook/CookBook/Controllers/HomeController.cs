﻿using BLL.Service;
using CookBook.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using static Model.Enums.CategoryEnum;

namespace CookBook.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return RedirectToAction("HomePage", "Home");
        }

        public ActionResult GetIngredientCategories()
        {
            var vm = new List<CategoryViewModel>();
            foreach (IngredientCategory item in Enum.GetValues(typeof(IngredientCategory)))
            {
                vm.Add(new CategoryViewModel { Value = (int)item, Name = item.ToString() });
            }

            return View(vm);
        }

        public ActionResult GetRecipeCategories()
        {
            var vm = new List<CategoryViewModel>();
            foreach (RecipeCategory item in Enum.GetValues(typeof(RecipeCategory)))
            {
                vm.Add(new CategoryViewModel {Value=(int)item, Name=item.ToString()});
            }

            return View(vm);
        }

        public ActionResult HomePage()
        {
            var recipeService = new RecipeService();
            var recipes = recipeService.GetLatestNine();
            var vm = new List<BasicRecipeDataViewModel>();
            foreach (var recipe in recipes)
            {
                vm.Add(new BasicRecipeDataViewModel(recipe));
            }

            return View(vm);
        }
    }
}
