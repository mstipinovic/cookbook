﻿using BLL.Service;
using CookBook.ViewModels;
using DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using static Model.Enums.CategoryEnum;

namespace CookBook.Controllers
{
    public class RecipeController : Controller
    {
        private RecipeService _service;

        public RecipeController()
        {
            _service = new RecipeService();
        }

        public ActionResult Index()
        {
            var recipes = _service.GetAll();
            var vm = new IndexRecipeViewModel();

            foreach (var recipe in recipes)
            {
                vm.Recipes.Add(new BasicRecipeDataViewModel(recipe));
            }

            return View(vm);
        }

        [HttpGet]
        public ActionResult Create()
        {
            if (Session["UserId"] == null)
                return RedirectToAction("Login", "Account");

            var vm = new RecipeViewModel();

            return View(vm);
        }

        [HttpPost]
        public ActionResult Create(RecipeViewModel vm)
        {
            if (!ModelState.IsValid)
                return View(vm);

            var recipeDto = new RecipeDto
            {
                Name = vm.Name,
                UserId = (int)Session["UserId"],
                Date = DateTime.Now
            };

            var recipeId = _service.Create(recipeDto);

            return RedirectToAction("Edit", "Recipe", new { id = recipeId });
        }

        public ActionResult Get(int id)
        {
            var recipe = _service.Get(id);
            var vm = new GetRecipeViewModel(recipe);
            
            return View(vm);
        }

        public ActionResult GetAllByCategory(int categoryId)
        {
            var recipes = _service.GetAllByCategory(categoryId);
            var vm = new RecipeCategoryViewModel();
            vm.Category = new CategoryViewModel { Name = Enum.GetName(typeof(RecipeCategory), categoryId) };
            foreach (var recipe in recipes)
            {
                vm.Recipes.Add(new RecipeViewModel(recipe));
            }

            return View(vm);
        }
        
        [HttpGet]
        public ActionResult GetRandomRecipe()
        {
            var vm = new GetRecipeViewModel();

            return View(vm);
        }

        [HttpPost]
        public ActionResult GetRandomRecipe(GetRecipeViewModel vm)
        {
            var recipe = new RecipeDto();
            if (Session["UserId"] != null)
                recipe = _service.Get(RandomRecipeAuthorized());
            else
                recipe = _service.Get(RandomRecipeUnauthorized());

            vm = new GetRecipeViewModel(recipe);

            return View(vm);
        }

        public int RandomRecipeAuthorized()
        {
            var userService = new UserService();
            var user = userService.Get((int)Session["UserId"]);
            var recipes = _service.GetAllIds().Where(x => !user.RandomRecipes.Contains(x)).ToList();
            Random rnd = new Random();
            var index = rnd.Next(recipes.Count);

            return recipes[index];
        }

        public int RandomRecipeUnauthorized()
        {
            var recipes = _service.GetAllIds();
            Random rnd = new Random();
            var index = rnd.Next(recipes.Count);

            return recipes[index];
        }

        public ActionResult Search(int filter, string search)
        {
            var recipes = new List<RecipeDto>();

            if (filter == 1)
                recipes = _service.SearchByName(search);
            else if (filter == 2)
                recipes = _service.SearchByCategory(search);

            var vm = new SearchRecipeViewModel();
            foreach (var recipe in recipes)
            {
                vm.Recipes.Add(new RecipeViewModel(recipe));
            }

            return View(vm);
        }

        [HttpGet]
        public ActionResult SearchByIngredients()
        {
            var vm = new List<RecipeViewModel>();

            return View(vm);
        }

        [HttpPost]
        public ActionResult SearchByIngredients(string search)
        {
            string[] separatingChars = { " ", ",", ", ", ".", "\t" };
            var ingredientNames = search.Split(separatingChars, System.StringSplitOptions.RemoveEmptyEntries);
            var recipes = _service.SearchByIngredients(ingredientNames);
            var vm = new List<RecipeViewModel>();
            foreach (var recipe in recipes)
            {
                vm.Add(new RecipeViewModel(recipe));
            }

            return View(vm);
        }

        [HttpPost]
        public ActionResult AddIngredient(EditRecipeViewModel vm)
        {
            vm.NewIngredient.RecipeId = vm.Recipe.Id;
            _service.AddIngredient(vm.NewIngredient);

            return RedirectToAction("Edit", "Recipe", new { id = vm.Recipe.Id });
        }

        public ActionResult RemoveIngredient(int ingredientId, int recipeId)
        {
            _service.RemoveIngredient(ingredientId, recipeId);

            return RedirectToAction("Edit", "Recipe", new { id = recipeId });
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            if (Session["UserId"] == null)
                return RedirectToAction("Login", "Account");

            var recipe = _service.Get(id);
            var vm = new EditRecipeViewModel
            {
                Recipe = recipe,
                Ingredients = IngredientsDropdownForEdit(recipe.Ingredients)
            };

            return View(vm);
        }
        
        private List<IngredientDto> IngredientsDropdownForEdit(List<RecipeIngredientsDto> recipeIngredients)
        {
            var serviceIngredient = new IngredientService();
            var dropdownList = serviceIngredient.GetIngredientsForEditRecipe(recipeIngredients);
       
            return dropdownList;
        }

        [HttpPost]
        public ActionResult Edit(EditRecipeViewModel vm)
        {
            vm.Ingredients = IngredientsDropdownForEdit(vm.Recipe.Ingredients);
            if (!ModelState.IsValid)
                return View(vm);

            if (vm.Recipe.Ingredients.Count == 0)
            {
                ModelState.AddModelError(string.Empty, "Recipe must have at least one ingredient!");
                return View(vm);
            }

            _service.Update(vm.Recipe);

            return RedirectToAction("GetAllUserRecipes", "User");
        }

        public ActionResult Delete(int id)
        {
            if (Session["UserId"] == null)
                return RedirectToAction("Login", "Account");

            var recipe = _service.Get(id);
            var vm = new GetRecipeViewModel(recipe);

            return View(vm);
        }

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            _service.Delete(id);

            return RedirectToAction("GetAllUserRecipes", "User");
        }

    }
}