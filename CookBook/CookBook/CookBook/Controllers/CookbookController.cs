﻿using BLL.Service;
using CookBook.ViewModels;
using DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace CookBook.Controllers
{
    public class CookbookController : Controller
    {
        private CookbookService _service;

        public CookbookController()
        {
            _service = new CookbookService();
        }

        public ActionResult Index()
        {
            var cookbooks = _service.GetAll();
            var vm = new List<CookbookViewModel>();
            foreach (var cookbook in cookbooks)
            {
                vm.Add(new CookbookViewModel { Cookbook = cookbook });
            }

            return View(vm);
        }

        [HttpGet]
        public ActionResult Create()
        {
            if (Session["UserId"] == null)
                return RedirectToAction("Login", "Account");

            var vm = new CookbookViewModel();

            return View(vm);
        }

        [HttpPost]
        public ActionResult Create(CookbookViewModel vm)
        {
            if (!ModelState.IsValid)
                return View(vm);

            var cookbookDto = new CookbookDto
            {
                Id = vm.Cookbook.Id,
                Name = vm.Cookbook.Name,
                Date = DateTime.Now,
                UserId = (int)Session["UserID"],
                Recipes = vm.Cookbook.Recipes
            };

            var cookbookId = _service.Create(cookbookDto);

            return RedirectToAction("Edit", "Cookbook", new { id = cookbookId });
        }

        public ActionResult Get(int id)
        {
            var cookbook = _service.Get(id);
            var vm = new CookbookViewModel {Cookbook=cookbook};

            return View(vm);
        }

        public ActionResult GetAvailabileRecipesForEditCookbook(int id)
        {
            var recipes = _service.GetAvailabileRecipesForEditCookbook(id);
            var vm = new CookbookViewModel { Cookbook = new CookbookDto { Id = id, Recipes=recipes } };

            return View(vm);
        }

        public ActionResult ShowRecipeForCookbook(int cookbookId, int recipeId)
        {
            var recipeService = new RecipeService();
            var vm = new GetCookbookViewModel
            {
                Recipe = recipeService.Get(recipeId),
                Cookbook=_service.Get(cookbookId)
            };

            return View(vm);            
        }

        public ActionResult AddRecipe(int cookbookId, int recipeId)
        {
            _service.AddRecipe(cookbookId, recipeId);

            return RedirectToAction("Edit","Cookbook", new {id=cookbookId});
        }

        public ActionResult RemoveRecipe(int cookbookId, int recipeId)
        {
            _service.RemoveRecipe(cookbookId, recipeId);

            return RedirectToAction("Edit", "Cookbook", new { id = cookbookId });
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            if (Session["UserId"] == null)
                return RedirectToAction("Login", "Account");

            var cookbook = _service.Get(id);
            var vm = new CookbookViewModel { Cookbook = cookbook };

            return View(vm);
        }

        [HttpPost]
        public ActionResult Edit(CookbookViewModel vm)
        {
            if (!ModelState.IsValid)
                return View(vm);

            if (vm.Cookbook.Recipes.Count == 0)
            {
                ModelState.AddModelError(string.Empty, "Cookbook must have at least one recipe!");
                return View(vm);
            }

            var cookbook = vm.Cookbook;
            _service.Update(cookbook);

            return RedirectToAction("GetAllUserCookbooks", "User");
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            if (Session["UserId"] == null)
                return RedirectToAction("Login", "Account");

            var cookbook = _service.Get(id);
            var vm = new CookbookViewModel { Cookbook = cookbook };

            return View(vm);
        }

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            _service.Delete(id);

            return RedirectToAction("GetAllUserCookbooks", "User");
        }
    }
}