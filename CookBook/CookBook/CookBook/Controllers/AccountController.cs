﻿using BLL.Service;
using CookBook.ViewModels;
using DTO;
using System.Linq;
using System.Web.Helpers;
using System.Web.Mvc;

namespace CookBook.Controllers
{
    public class AccountController : Controller
    {
        private UserService _userService;

        public AccountController()
        {
            _userService = new UserService();
        }

        [HttpGet]
        public ActionResult Login()
        {
            var vm = new AccountViewModel();

            return View(vm);
        }

        [HttpPost]
        public ActionResult Login(AccountViewModel vm)
        {
            if (!ModelState.IsValid)
                return View(vm);

            var user = _userService.Get(vm.Username);
            if (user.Username == null || !Crypto.VerifyHashedPassword(user.Password, vm.Password))
            {
                ModelState.AddModelError(string.Empty, "Incorrect user data!");
                return View(vm);
            }

            Session["UserId"] = user.Id;

            return RedirectToAction("GetProfile","User", new { id=user.Id});
        }

        public ActionResult Logout()
        {
            Session["UserId"] = null;

            return RedirectToAction("HomePage", "Home");
        }

        [HttpGet]
        public ActionResult Registration()
        {
            var vm = new AccountViewModel();

            return View(vm);
        }

        [HttpPost]
        public ActionResult Registration(AccountViewModel vm)
        {
            if (!ModelState.IsValid)
                return View(vm);

            var user = _userService.CheckIfExist(vm.Username, -1);
            if (user.Any())
            {
                ModelState.AddModelError(string.Empty, "User with selected username already exist");
                return View(vm);
            }

            var newUser = new UserDto
            {
                Username = vm.Username,
                Password = Crypto.HashPassword(vm.Password)
            };

            _userService.Create(newUser);

            return RedirectToAction("Login", "Account");
        }
    }
}