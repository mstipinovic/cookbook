﻿using DTO;
using Model;
using System.Collections.Generic;

namespace BLL.Converter
{
    public class RecipeIngredientsConverter
    {
        private IngredientConverter _ingredientConverter;

        public RecipeIngredientsConverter()
        {
            _ingredientConverter = new IngredientConverter();
        }

        public RecipeIngredientsDto ModelToDto(RecipeIngredients model)
        {
            var dto = new RecipeIngredientsDto();
            if (model == null)
                return dto;

            dto.IngredientId = model.IngredientId;
            dto.RecipeId = model.RecipeId;
            dto.Amount = model.Amount;

            if (model.Ingredient!=null)
            {
                dto.Ingredient = _ingredientConverter.ModelToDto(model.Ingredient);
            }

            return dto;
        }

        public List<RecipeIngredientsDto> ListToDto(List<RecipeIngredients> model)
        {
            var dto = new List<RecipeIngredientsDto>();
            if (model == null)
                return dto;

            foreach (var ingredient in model)
            {
                dto.Add(ModelToDto(ingredient));
            }

            return dto;
        }

        public RecipeIngredients DtoToModel(RecipeIngredientsDto dto)
        {
            var model = new RecipeIngredients();
            if (dto == null)
                return model;

            model.IngredientId = dto.IngredientId;
            model.RecipeId = dto.RecipeId;
            model.Amount = dto.Amount;

            return model;
        }

        public List<RecipeIngredients> ListToModel(List<RecipeIngredientsDto> dto)
        {
            var model = new List<RecipeIngredients>();
            if (dto == null)
                return model;

            foreach (var ingredient in dto)
            {
                model.Add(DtoToModel(ingredient));
            }

            return model;
        }
    }
}
