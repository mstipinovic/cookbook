﻿using DTO;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Converter
{
    public class IngredientConverter
    {
        public IngredientDto ModelToDto(Ingredient model)
        {
            var dto = new IngredientDto();
            if (model == null)
                return dto;

            dto.Id = model.Id;
            dto.Name = model.Name;
            dto.Measure = model.Measure;
            dto.Category = model.Category;

            return dto;
        }

        public List<IngredientDto> ListToDto(List<Ingredient> model)
        {
            var dto = new List<IngredientDto>();
            if (model == null)
                return dto;

            foreach (var ingredient in model)
            {
                dto.Add(ModelToDto(ingredient));
            }

            return dto;
        }

        public Ingredient DtoToModel(IngredientDto dto)
        {
            var model = new Ingredient();
            if (dto == null)
                return model;

            model.Id = dto.Id;
            model.Name = dto.Name;
            model.Measure = dto.Measure;
            model.Category = dto.Category;

            return model;
        }
    }
}
