﻿using BLL.Service;
using DTO;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Converter
{
   public class CookbookConverter
    {
        private RecipeConverter _recipeConverter;

        public CookbookConverter()
        {
            _recipeConverter = new RecipeConverter();
        }
     
        public CookbookDto ModelToDto(Cookbook model)
        {
            var userService = new UserService();
            var recipeService = new RecipeService();

            var dto = new CookbookDto();
            if (model == null)
                return dto;

            dto.Id = model.Id;
            dto.Name = model.Name;
            dto.Date = model.Date;
            dto.UserId = model.UserId;
            dto.Username = userService.GetUsername(model.UserId);

            if (model.Recipes!=null)
            {
                var recipes = model.Recipes.Where(x => x.CookbookId == model.Id).Select(x => x.RecipeId).ToList();
                dto.Recipes = recipeService.GetAllByListOfIds(recipes);
            }

            return dto;
        }

        public List<CookbookDto> ListoToDto(List<Cookbook> model)
        {
            var dto = new List<CookbookDto>();
            if (model == null)
                return dto;

            foreach (var cookbook in model)
            {
                dto.Add(ModelToDto(cookbook));
            }

            return dto;
        }

        public Cookbook DtoToModel(CookbookDto dto)
        {
            var model = new Cookbook();
            if (dto == null)
                return model;

            model.Id = dto.Id;
            model.Name = dto.Name;
            model.Date = dto.Date;
            model.UserId = dto.UserId;

            model.Recipes = new List<CookbookRecipes>();
            foreach (var recipe in dto.Recipes)
            {
                model.Recipes.Add(new CookbookRecipes { CookbookId=dto.Id, RecipeId=recipe.Id});
            }

            return model;
        }

        public List<Cookbook> ListToModel(List<CookbookDto> dto)
        {
            var model = new List<Cookbook>();
            if (dto == null)
                return model;

            foreach (var cookbook in dto)
            {
                model.Add(DtoToModel(cookbook));
            }

            return model;
        }
    }
}
