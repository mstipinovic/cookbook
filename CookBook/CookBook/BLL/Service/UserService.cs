﻿using BLL.Converter;
using DAL.Repository;
using DTO;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BLL.Service
{
    public class UserService
    {
        private UserRepository _repository;
        private UserConverter _converter;

        public UserService()
        {
            _repository = new UserRepository();
            _converter = new UserConverter();
        }

        public void Create(UserDto userDto)
        {
            var user = _converter.DtoToModel(userDto);
            _repository.Create(user);
        }

        public UserDto Get(int id)
        {
            var user = _repository.Get(id);
            var userDto = _converter.ModelToDto(user);

            return userDto;
        }

        public UserDto Get(string username)
        {
            var user = _repository.Get(username);
            var userDto = _converter.ModelToDto(user);

            return userDto;
        }

        public string GetUsername(int id)
        {
            var username = _repository.GetUsername(id);

            return username;
        }

        public List<UserDto> GetAll()
        {
            var users = _repository.GetAll();
            var usersDto = _converter.ListToDto(users);

            return usersDto;
        }

        public List<UserDto> CheckIfExist(string username, int id)
        {
            var user = _repository.CheckIfExist(username, id);
            var userDto = _converter.ListToDto(user);

            return userDto;
        }

        public void AddRandomRecipe(int userId, int recipeId)
        {
            var user =_converter.ModelToDto( _repository.Get(userId));
            if (user.RandomRecipes.Count >= 7)
                DeleteOldestRandomRecipe();

            var userRecipe = _repository.GetUserRecipe(userId, recipeId);

            if (userRecipe==null)
            {
                var randomRecipe = new UserRecipes { UserId = userId, RecipeId = recipeId, Random = true };
                _repository.AddUserRecipe(randomRecipe);
            }
            else
            {
                userRecipe.Random = true;
                _repository.UpdateUserRecipe(userRecipe);
            }
        }

        public void DeleteOldestRandomRecipe()
        {
           var oldestRecipe= _repository.GetOldestUserRecipe();

            oldestRecipe.Random = false;

            if (oldestRecipe.Favorite == false)
                _repository.RemoveUserRecipe(oldestRecipe);
            else
                _repository.UpdateUserRecipe(oldestRecipe);

        }

        public void AddFavoriteRecipe(int userId, int recipeId)
        {
            var userRecipe = _repository.GetUserRecipe(userId, recipeId);

            if (userRecipe == null)
            {
                var favoriteRecipe = new UserRecipes { UserId = userId, RecipeId = recipeId, Favorite = true };
                _repository.AddUserRecipe(favoriteRecipe);
            }
            else
            {
                userRecipe.Favorite = true;
                _repository.UpdateUserRecipe(userRecipe);
            }
        }

        public void RemoveFavoriteRecipe(int userId, int recipeId)
        {
            var userRecipe = _repository.GetUserRecipe(userId, recipeId);

            userRecipe.Favorite = false;

            if (userRecipe.Random == false)
                _repository.RemoveUserRecipe(userRecipe);
            else
                _repository.UpdateUserRecipe(userRecipe);
        }

        public void Update(UserDto userDto)
        {
            var user = _converter.DtoToModel(userDto);
            user.Recipes = user.Recipes.OrderByDescending(x => x.Date).ToList();

            _repository.Update(user);
        }

        public void Delete(int id)
        {
             _repository.Delete(id);
        }
    }
}
