﻿using BLL.Converter;
using DAL.Repository;
using DTO;
using System.Collections.Generic;
using System.Linq;

namespace BLL.Service
{
    public class RecipeService
    {
        private RecipeRepository _repository;
        private RecipeConverter _converter;

        public RecipeService()
        {
            _repository = new RecipeRepository();
            _converter = new RecipeConverter();
        }

        public int Create(RecipeDto recipeDto)
        {
            var recipe = _converter.DtoToModel(recipeDto);
            _repository.Create(recipe);

            return recipe.Id;
        }

        public RecipeDto Get(int id)
        {
            var recipe = _repository.Get(id);
            var recipeDto = _converter.ModelToDto(recipe);

            return recipeDto;
        }

        public List<RecipeDto> GetLatestNine()
        {
            var recipes = _repository.GetLatestNineRecipes();
            var recipesDto = _converter.ListToDto(recipes);

            return recipesDto;
        }
        
        public List<RecipeDto> GetAll()
        {
            var recipes = _repository.GetAll();
            var recipesDto = _converter.ListToDto(recipes);

            return recipesDto;
        }

        public List<int> GetAllIds()
        {
            var recipes = _repository.GetAllIds();

            return recipes;
        }
        
        public List<RecipeDto> GetAllByCategory(int categoryId)
        {
            var recipes = _repository.GetAllByCategory(categoryId);
            var recipesDto = _converter.ListToDto(recipes);

            return recipesDto;
        }
        
        public List<RecipeDto> GetAllByListOfIds(List<int> ids)
        {
            var recipes = _repository.GetAllByListOfIds(ids);
            var recipesDto = _converter.ListToDto(recipes);

            return recipesDto;
        }

        public List<RecipeDto> SearchByIngredients(string[] ingredientNames)
        {
            var recipes = _repository.SearchByIngredients(ingredientNames);
            var recipesDto = _converter.ListToDto(recipes);

            return recipesDto;
        }

        public List<RecipeDto> SearchByCategory(string search)
        {
            var recipeIdsByRecipeCategory = _repository.SearchByCategory(search).Select(x=>x.Id).ToList();
            var recipeIdsByIngredientCategory = _repository.SearchRecipeIngredientsCategory(search).Select(x => x.RecipeId).ToList();

            recipeIdsByRecipeCategory.AddRange(recipeIdsByIngredientCategory);
            recipeIdsByRecipeCategory = recipeIdsByRecipeCategory.Distinct().ToList();

            var recipes = _repository.GetAllByListOfIds(recipeIdsByRecipeCategory);
            var recipesDto = _converter.ListToDto(recipes);

            return recipesDto;
        }

        public List<RecipeDto> SearchByName(string search)
        {
            var recipes = _repository.SearchByName(search);
            var recipesDto = _converter.ListToDto(recipes);

            return recipesDto;
        }

        public void AddIngredient(RecipeIngredientsDto newIngredient)
        {
            var converter = new RecipeIngredientsConverter();
            var ingredient = converter.DtoToModel(newIngredient);

            _repository.AddIngredient(ingredient);
        }

        public void RemoveIngredient(int ingredientId, int recipeId)
        {
            _repository.RemoveIngredient(ingredientId, recipeId);
        }

        public void Update(RecipeDto recipeDto)
        {
            var recipe = _converter.DtoToModel(recipeDto);
            var recipeCategorySetter = new RecipeCategorySeter();
            recipe.Category = recipeCategorySetter.Set(recipeDto.Ingredients);

            _repository.Update(recipe);
        }

        public void Delete(int id)
        {
            _repository.Delete(id);
        }
    }
}
