﻿using BLL.Converter;
using DAL.Repository;
using DTO;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BLL.Service
{
    public class CookbookService
    {
        private CookbookRepository _repository;
        private CookbookConverter _converter;

        public CookbookService()
        {
            _repository = new CookbookRepository();
            _converter = new CookbookConverter();
        }

        public int Create(CookbookDto cookbookDto)
        {
            var cookbook = _converter.DtoToModel(cookbookDto);
            _repository.Create(cookbook);

            return cookbook.Id;
        }

        public CookbookDto Get(int id)
        {
            var cookbook = _repository.Get(id);
            var cookbookDto = _converter.ModelToDto(cookbook);

            return cookbookDto;
        }

        public List<CookbookDto> GetAll()
        {
            var cookbooks = _repository.GetAll();
            var cookbooksDto = _converter.ListoToDto(cookbooks);

            return cookbooksDto;
        }

        public List<CookbookDto> GetAllByUser(int userId)
        {
            var cookbooks = _repository.GetAllByUser(userId);
            var cookbooksDto = _converter.ListoToDto(cookbooks);

            return cookbooksDto;
        }

        public List<RecipeDto> GetAvailabileRecipesForEditCookbook(int id)
        {
            var recipeService = new RecipeService();

            var cookbookRecipes = _repository.GetAllCookbookRecipes(id);
            var recipes = recipeService.GetAll().Where(x => !cookbookRecipes.Contains(x.Id)).ToList();

            return recipes;
        }

        public void AddRecipe(int cookbookId, int recipeId)
        {
            var recipe = new CookbookRecipes { CookbookId = cookbookId, RecipeId = recipeId };
            _repository.AddRecipe(recipe);
        }

        public void RemoveRecipe(int cookbookId, int recipeId)
        {
            _repository.RemoveRecipe(cookbookId, recipeId);
        }

        public void Update(CookbookDto cookbookDto)
        {
            var cookbook = _converter.DtoToModel(cookbookDto);
            _repository.Update(cookbook);
        }

        public void Delete(int id)
        {
            _repository.Delete(id);
        }
    }
}
