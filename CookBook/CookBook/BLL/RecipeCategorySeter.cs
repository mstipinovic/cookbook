﻿using BLL.Service;
using DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Model.Enums.CategoryEnum;

namespace BLL
{
    class RecipeCategorySeter
    {

        public RecipeCategory Set(List<RecipeIngredientsDto> recipeIngredients)
        {
            var ingredientService = new IngredientService();
            List<int> ids = recipeIngredients.Select(x => x.IngredientId).Distinct().ToList();
            var ingredients = ingredientService.GetByListOfIds(ids);
            var category = new RecipeCategory();

            foreach (var ingredient in ingredients)
            {
                if (ingredient.Category == IngredientCategory.meat || ingredient.Category == IngredientCategory.seaFood)
                {
                    category = RecipeCategory.regular;
                    return category;
                }
                else if (ingredient.Category == IngredientCategory.animalOrigin || ingredient.Category == IngredientCategory.dairyProduct)
                {
                    category = RecipeCategory.vegetarian;
                }
                else
                {
                    if (!(category == RecipeCategory.vegetarian))
                    {
                        category = RecipeCategory.vegan;
                    }
                }
            }

            return category;
        }
    }
}

