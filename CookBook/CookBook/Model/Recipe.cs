﻿using Model.Enums;
using System;
using System.Collections.Generic;
using static Model.Enums.CategoryEnum;

namespace Model
{
    public class Recipe
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int UserId { get; set; }
        public DateTime Date { get; set; }
        public RecipeCategory Category{ get; set; }

        public virtual User User { get; set; }
        public virtual List<RecipeIngredients> Ingredients { get; set; }
    }
}
