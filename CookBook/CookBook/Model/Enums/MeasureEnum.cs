﻿using System.ComponentModel.DataAnnotations;

namespace Model.Enums
{
    public class MeasureEnum
    {
        public enum Measure
        {
            [Display(Name="Teaspoon")]
            teaspoon,
            [Display(Name = "Tablespoon")]
            tablespoon,
            [Display(Name = "Cup")]
            cup,
            [Display(Name = "Mililiter")]
            mililiter,
            [Display(Name = "Gram")]
            gram,
            [Display(Name = "Piece")]
            piece,
        }
    }
}
