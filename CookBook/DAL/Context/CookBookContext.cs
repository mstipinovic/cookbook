﻿using Model;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace DAL.Context
{
    public class CookBookContext : DbContext
    {
        public CookBookContext() : base("CookBookContext")
        {
            this.Configuration.ProxyCreationEnabled = false;
        }

        public virtual DbSet<Ingredient> Ingredients { get; set; }
        public virtual DbSet<Recipe> Recipes { get; set; }
        public virtual DbSet<RecipeIngredients> RecipeIngredients { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<UserRecipes> UserRecipes { get; set; }
        public virtual DbSet<Cookbook> Cookbooks { get; set; }
        public virtual DbSet<CookbookRecipes> CookbookRecipes { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder) => modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
    }
}
