﻿using DAL.Context;
using DTO;
using Model;
using Model.Enums;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace DAL.Repository
{
   public class IngredientRepository
    {
        public void Create(Ingredient ingredient)
        {
            using (var context=new CookBookContext())
            {
                context.Ingredients.Add(ingredient);
                context.SaveChanges();
            }
        }

        public Ingredient Get(int id)
        {
            using (var context = new CookBookContext())
            {
                return context.Ingredients.FirstOrDefault(x=>x.Id==id);
            }
        }

        public List<Ingredient> GetAll()
        {
            using (var context = new CookBookContext())
            {
                return context.Ingredients.OrderBy(x=>x.Name.ToLower()).ToList();
            }
        }

        public List<Ingredient> GetByCategory(int categoryId)
        {
            using (var context = new CookBookContext())
            {
                return context.Ingredients.Where(x => ((int)x.Category).Equals(categoryId)).OrderBy(x => x.Name.ToLower()).ToList();
            }
        }

        public List<Ingredient> GetByListOfIds(List<int> ids)
        {
            using (var context = new CookBookContext())
            {
                return context.Ingredients.Where(x => ids.Contains(x.Id)).ToList();
            }
        }

        public List<Ingredient> GetIngredientsForEditRecipe(List<RecipeIngredientsDto> recipeIngredients)
        {
            using (var context=new CookBookContext())
            {
                var ingredientsIds = recipeIngredients.Select(x=>x.IngredientId).ToList();
                var ingredients = context.Ingredients.Where(x=> ingredientsIds.Contains(x.Id));
                var ingredientsForEdit = context.Ingredients.AsEnumerable().Except(ingredients).ToList();

                return ingredientsForEdit;
            }
        }

        public List<Ingredient> GetByListOfNames(string[] search)
        {
            using (var context = new CookBookContext())
            {
                return context.Ingredients.Where(x => search.Contains(x.Name)).ToList();
            }
        }

        public List<Ingredient> CheckIfExist(int id, string name, MeasureEnum.Measure measure)
        {
            using (var context = new CookBookContext())
            {
                return context.Ingredients.Where(x => x.Name.Equals(name) && x.Measure == measure && x.Id != id).ToList();
            }
        }

        public void Update(Ingredient ingredient)
        {
            using (var context = new CookBookContext())
            {
                context.Entry(ingredient).State = EntityState.Modified;
                context.SaveChanges();
            }
        }

        public void Delete(int id)
        {
            using (var context = new CookBookContext())
            {
                var ingredient = context.Ingredients.Single(x => x.Id == id);
                context.Entry(ingredient).State = EntityState.Deleted;
                context.SaveChanges();
            }
        }
    }
}
