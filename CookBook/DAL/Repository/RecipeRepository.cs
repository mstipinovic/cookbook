﻿using DAL.Context;
using Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace DAL.Repository
{
    public class RecipeRepository
    {
        public void Create(Recipe recipe)
        {
            using (var context = new CookBookContext())
            {
                context.Recipes.Add(recipe);
                context.SaveChanges();
            }
        }

        public Recipe Get(int id)
        {
            using (var context = new CookBookContext())
            {
                var recipe = context.Recipes.Include("Ingredients").Include("Ingredients.Ingredient").FirstOrDefault(x => x.Id == id);

                return recipe;
            }
        }

        public List<Recipe> GetLatestNineRecipes()
        {
            using (var context = new CookBookContext())
            {
                return context.Recipes.Take(9).OrderByDescending(x => x.Date).ToList();
            }
        }

        public List<Recipe> GetAll()
        {
            using (var context = new CookBookContext())
            {
                return context.Recipes.OrderBy(x => x.Name).ToList();
            }
        }

        public List<int> GetAllIds()
        {
            using (var context = new CookBookContext())
            {
                return context.Recipes.Select(x => x.Id).ToList();
            }
        }

        public List<Recipe> GetAllByCategory(int categoryId)
        {
            using (var context = new CookBookContext())
            {
                return context.Recipes.Where(x => ((int)x.Category).Equals(categoryId)).OrderBy(x => x.Name.ToLower()).ToList();
            }
        }

        public List<Recipe> GetAllByListOfIds(List<int> recipeIds)
        {
            using (var context = new CookBookContext())
            {
                return context.Recipes.Where(x => recipeIds.Contains(x.Id)).OrderBy(x=>x.Name.ToLower()).ToList();
            }
        }

        public List<Recipe> SearchByIngredients(string[] ingredientNames)
        {
            using (var context = new CookBookContext())
            {
                var ing = context.Ingredients.Where(x => ingredientNames.Contains(x.Name)).Select(x => x.Id);
                var recipes = context.Recipes.Where(x => !ing.Except(x.Ingredients.Select(r => r.IngredientId)).Any()).ToList();

                return recipes;
            }
        }

        public List<Recipe> SearchByCategory(string search)
        {
            using (var context = new CookBookContext())
            {
                return context.Recipes.Where(x => x.Category.ToString().Contains(search)).OrderBy(x=>x.Name.ToLower()).ToList();
            }
        }

        public List<Recipe> SearchByName(string search)
        {
            using (var context = new CookBookContext())
            {
                return context.Recipes.Where(x => x.Name.Contains(search)).OrderBy(x=>x.Name.ToLower()).ToList();
            }
        }

        public List<RecipeIngredients> SearchRecipeIngredientsCategory(string search)
        {
            using (var context = new CookBookContext())
            {
                return context.RecipeIngredients.Where(x => x.Ingredient.Category.ToString().Contains(search)).ToList();
            }
        }

        public void AddIngredient(RecipeIngredients ingredient)
        {
            using (var context = new CookBookContext())
            {
                context.RecipeIngredients.Add(ingredient);
                context.SaveChanges();
            }
        }

        public void RemoveIngredient(int ingredientId, int recipeId)
        {
            using (var context = new CookBookContext())
            {
                var ingredient = context.RecipeIngredients.SingleOrDefault(x => x.IngredientId == ingredientId && x.RecipeId == recipeId);
                context.Entry(ingredient).State = EntityState.Deleted;
                context.SaveChanges();
            }
        }

        public void Update(Recipe recipe)
        {
            using (var context = new CookBookContext())
            {
                context.Entry(recipe).State = EntityState.Modified;
                context.SaveChanges();
            }
        }

        public void Delete(int id)
        {
            using (var context = new CookBookContext())
            {
                var recipe = context.Recipes.Single(x => x.Id == id);
                context.Entry(recipe).State = EntityState.Deleted;
                context.SaveChanges();
            }
        }
    }
}
