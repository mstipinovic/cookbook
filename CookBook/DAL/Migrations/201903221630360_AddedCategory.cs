namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedCategory : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Ingredient", "Category", c => c.Int(nullable: false));
            AddColumn("dbo.Recipe", "Category", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Recipe", "Category");
            DropColumn("dbo.Ingredient", "Category");
        }
    }
}
