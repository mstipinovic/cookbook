namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedUserRecipeId : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.UserRecipes");
            AddColumn("dbo.UserRecipes", "Id", c => c.Int(nullable: false, identity: true));
            AddPrimaryKey("dbo.UserRecipes", "Id");
        }
        
        public override void Down()
        {
            DropPrimaryKey("dbo.UserRecipes");
            DropColumn("dbo.UserRecipes", "Id");
            AddPrimaryKey("dbo.UserRecipes", new[] { "UserId", "RecipeId" });
        }
    }
}
