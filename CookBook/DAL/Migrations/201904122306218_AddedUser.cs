namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedUser : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.User",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Username = c.String(),
                        Password = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Recipe", "UserId", c => c.Int(nullable: false));
            CreateIndex("dbo.Recipe", "UserId");
            AddForeignKey("dbo.Recipe", "UserId", "dbo.User", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Recipe", "UserId", "dbo.User");
            DropIndex("dbo.Recipe", new[] { "UserId" });
            DropColumn("dbo.Recipe", "UserId");
            DropTable("dbo.User");
        }
    }
}
