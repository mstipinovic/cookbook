﻿using System.Collections.Generic;

namespace DTO
{
    public class UserDto
    {
        public UserDto()
        {
            Recipes = new List<RecipeDto>();
            RandomRecipes = new List<int>();
            FavoriteRecipes = new List<RecipeDto>();
            Cookbooks = new List<CookbookDto>();
        }

        public int Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public List<RecipeDto> Recipes { get; set; }
        public List<int> RandomRecipes { get; set; }
        public List<RecipeDto> FavoriteRecipes { get; set; }
        public List<CookbookDto> Cookbooks { get; set; }
    }
}
