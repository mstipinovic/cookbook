﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using static Model.Enums.CategoryEnum;

namespace DTO
{
    public class RecipeDto
    {
        public RecipeDto()
        {
            Ingredients = new List<RecipeIngredientsDto>();
        }

        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required, StringLength(255, MinimumLength = 1)]
        public string Description { get; set; }
        public RecipeCategory Category { get; set; }
        public int UserId { get; set; }
        public string Username { get; set; }
        public DateTime Date { get; set; }
        public List<RecipeIngredientsDto> Ingredients { get; set; }
    }
}
