﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using static Model.Enums.CategoryEnum;
using static Model.Enums.MeasureEnum;

namespace DTO
{
    public class IngredientDto
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public Measure Measure { get; set; }
        public IngredientCategory Category { get; set; }
    }
}
