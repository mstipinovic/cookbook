﻿using System.Collections.Generic;
using static Model.Enums.CategoryEnum;
using static Model.Enums.MeasureEnum;

namespace Model
{
    public class Ingredient
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public Measure Measure { get; set; }
        public IngredientCategory Category { get; set; }
    }
}
