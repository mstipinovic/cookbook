﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class CookbookRecipes
    {
        [Key, Column(Order = 0)]
        public int CookbookId { get; set; }
        [Key, Column(Order = 1)]
        public int RecipeId { get; set; }

        public virtual Cookbook Cookbook { get; set; }
        public virtual Recipe Recipe { get; set; }
    }
}
