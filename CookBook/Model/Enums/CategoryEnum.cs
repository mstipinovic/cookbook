﻿using System.ComponentModel.DataAnnotations;


namespace Model.Enums
{
    public class CategoryEnum
    {
        public enum RecipeCategory
        {
            [Display(Name="vegan")]
            vegan,
            [Display(Name = "vegetarian")]
            vegetarian,
            [Display(Name = "regular")]
            regular
        }

        public enum IngredientCategory
        {
            [Display(Name = "Meat")]
            meat,
            [Display(Name = "Animal Origin")]
            animalOrigin,
            [Display(Name = "Vegetable")]
            vegetable,
            [Display(Name = "Fruit")]
            fruit,
            [Display(Name = "DairyProduct")]
            dairyProduct,
            [Display(Name = "SeaFood")]
            seaFood,
            [Display(Name = "Grains")]
            grains,
            [Display(Name = "Other")]
            other
        }
    }
}
