﻿using System;
using System.Collections.Generic;

namespace Model
{
   public class Cookbook
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime Date { get; set; }
        public int UserId { get; set; }

        public virtual List<CookbookRecipes> Recipes { get; set; }
    }
}
