﻿using System.Collections.Generic;

namespace Model
{
    public class User
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
 
        public virtual List<UserRecipes> RandomAndFavoriteRecipes { get; set; }
        public virtual List<Recipe> Recipes { get; set;}
        public virtual List<Cookbook> Cookbooks { get; set; }
    }
}
