﻿using BLL.Service;
using DTO;
using Model;
using System.Collections.Generic;

namespace BLL.Converter
{
    public class RecipeConverter
    {
        private RecipeIngredientsConverter _recipeIngredientsConverter;

        public RecipeConverter()
        {
            _recipeIngredientsConverter = new RecipeIngredientsConverter();
        }

        public RecipeDto ModelToDto(Recipe model)
        {
            var userService = new UserService();

            var dto = new RecipeDto();
            if (model == null)
                return dto;

            dto.Id = model.Id;
            dto.Name = model.Name;
            dto.Description = model.Description;
            dto.Category = model.Category;
            dto.UserId = model.UserId;
            dto.Date = model.Date;
            dto.Username = userService.GetUsername(model.UserId);

            if (model.Ingredients != null)
            {
                dto.Ingredients = _recipeIngredientsConverter.ListToDto(model.Ingredients);
            }

            return dto;
        }

        public List<RecipeDto> ListToDto(List<Recipe> model)
        {
            var dto = new List<RecipeDto>();
            if (model == null)
                return dto;

            foreach (var recipe in model)
            {
                dto.Add(ModelToDto(recipe));
            }

            return dto;
        }

        public Recipe DtoToModel(RecipeDto dto)
        {
            var model = new Recipe();
            if (dto == null)
                return model;

            model.Id = dto.Id;
            model.Name = dto.Name;
            model.Description = dto.Description;
            model.Category = dto.Category;
            model.UserId = dto.UserId;
            model.Date = dto.Date;
            model.Ingredients = _recipeIngredientsConverter.ListToModel(dto.Ingredients);

            return model;
        }

        public List<Recipe> ListToModel(List<RecipeDto> dto)
        {
            var model = new List<Recipe>();
            if (dto == null)
                return null;

            foreach (var recipe in dto)
            {
                model.Add(DtoToModel(recipe));
            }

            return model;
        }
    }
}
