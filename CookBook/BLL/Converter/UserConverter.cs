﻿using BLL.Service;
using DTO;
using Model;
using System.Collections.Generic;
using System.Linq;

namespace BLL.Converter
{
    public class UserConverter
    {
        private RecipeConverter _recipeConverter;

        public UserConverter()
        {
            _recipeConverter = new RecipeConverter();
        }

        public UserDto ModelToDto(User model)
        {
            var service = new RecipeService();
            var cookbookConverter = new CookbookConverter();

            var dto = new UserDto();
            if (model == null)
                return dto;

            dto.Id = model.Id;
            dto.Username = model.Username;
            dto.Password = model.Password;

            if (model.Recipes != null)
            {
                dto.Recipes = _recipeConverter.ListToDto(model.Recipes).OrderByDescending(x => x.Date).ToList();
            }

            if (model.RandomAndFavoriteRecipes != null)
            {
                dto.RandomRecipes = model.RandomAndFavoriteRecipes.Where(x=>x.Random==true).Select(x => x.RecipeId).ToList();

                var favorites = model.RandomAndFavoriteRecipes.Where(x => x.Favorite == true).Select(x => x.RecipeId).ToList();
                dto.FavoriteRecipes = service.GetAllByListOfIds(favorites);
            }

            if (model.Cookbooks!=null)
            {
                dto.Cookbooks = cookbookConverter.ListoToDto(model.Cookbooks);
            }

            return dto;
        }

        public List<UserDto> ListToDto(List<User> model)
        {
            var dto = new List<UserDto>();
            if (model == null)
                return dto;

            foreach (var user in model)
            {
                dto.Add(ModelToDto(user));
            }

            return dto;
        }

        public User DtoToModel(UserDto dto)
        {
            var model = new User();
            if (dto == null)
                return model;

            model.Id = dto.Id;
            model.Username = dto.Username;
            model.Password = dto.Password;
            model.Recipes = _recipeConverter.ListToModel(dto.Recipes);

            model.RandomAndFavoriteRecipes = new List<UserRecipes>();
            foreach (var recipe in dto.RandomRecipes)
            {
                model.RandomAndFavoriteRecipes.Add(new UserRecipes { UserId = dto.Id, RecipeId = recipe, Random=true});
            }

            model.RandomAndFavoriteRecipes = new List<UserRecipes>();
            foreach (var recipe in dto.FavoriteRecipes)
            {
                model.RandomAndFavoriteRecipes.Add(new UserRecipes { UserId = dto.Id, RecipeId = recipe.Id, Favorite=true});
            }

            return model;
        }
    }
}
