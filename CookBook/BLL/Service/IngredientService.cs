﻿using BLL.Converter;
using DAL.Repository;
using DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using static Model.Enums.MeasureEnum;

namespace BLL.Service
{
    public class IngredientService
    {
        private IngredientRepository _repository;
        private IngredientConverter _converter;

        public IngredientService()
        {
            _repository = new IngredientRepository();
            _converter = new IngredientConverter();
        }

        public void Create(IngredientDto ingredientDto)
        {
            var ingredient = _converter.DtoToModel(ingredientDto);

            _repository.Create(ingredient);
        }

        public IngredientDto Get(int id)
        {
            var ingredient = _repository.Get(id);
            var ingredientDto = _converter.ModelToDto(ingredient);

            return ingredientDto;
        }

        public List<IngredientDto> GetAll()
        {
            var ingredients = _repository.GetAll();
            var ingredientsDto = _converter.ListToDto(ingredients);

            return ingredientsDto;
        }

        public List<IngredientDto> GetIngredientsForEditRecipe(List<RecipeIngredientsDto> recipeIngredients)
        {
            var ingredients = _repository.GetIngredientsForEditRecipe(recipeIngredients);
            var ingredientsDto = _converter.ListToDto(ingredients);

            return ingredientsDto;
        }

        public List<IngredientDto> GetByCategory(int categoryId)
        {
            var ingredients = _repository.GetByCategory(categoryId);
            var ingredientsDto = _converter.ListToDto(ingredients);

            return ingredientsDto;
        }

        public List<IngredientDto> GetByListOfIds(List<int> ids)
        {
            var ingredients = _repository.GetByListOfIds(ids);
            var ingredientsDto = _converter.ListToDto(ingredients);

            return ingredientsDto;
        }

        public List<IngredientDto> GetByListOfNames(string[] search)
        {
            var ingredients = _repository.GetByListOfNames(search);
            var ingredientsDto = _converter.ListToDto(ingredients);

            return ingredientsDto;
        }

        public List<IngredientDto> CheckIfExist(int id, string name, Measure measure)
        {
            var ingredients = _repository.CheckIfExist(id, name, measure);
            var ingredientsDto = _converter.ListToDto(ingredients);

            return ingredientsDto;
        }

        public void Update(IngredientDto ingredientDto)
        {
            var ingredient = _converter.DtoToModel(ingredientDto);

            _repository.Update(ingredient);
        }

        public void Delete(int id)
        {
            _repository.Delete(id);
        }
    }
}
